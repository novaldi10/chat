(function(){

    const app = document.querySelector(".app");
    const socket = io();

    let uname;

    // ketika tombol nya diklick maka functionnya / atau apa yang terjadi jika diklick istilahnya
    app.querySelector(".join-screen #join-user").addEventListener("click", function(){
            let username = app.querySelector(".join-screen #username").value;
            // kalau username panjangnya 0/tidak ada maka enggak meng emit apapun
            if(username.length == 0){
                return;
            }
            uname = username;

            // fungsi socket.io untuk mengirim data ke index.js dan udah nyambung di socket.io
            socket.emit("newuser",username);

            // memunculkan chat screen nya / untuk meng switch tampilan awal menjadi tampilan kolom chat
            app.querySelector(".join-screen").classList.remove("active");
            app.querySelector(".chat-screen").classList.add("active");
    });

    // untuk membuat tombol kembali/exit berfungsi
    app.querySelector(".chat-screen #exit-chat").addEventListener("click", function(){
        // tinggal merubah posisi add dengan removenya
        app.querySelector(".join-screen").classList.add("active");
        app.querySelector(".chat-screen").classList.remove("active");
    });

    // untuk tombol send pada chat nya (function untuk callback nya)
    app.querySelector(".chat-screen #send-message").addEventListener('click', function(){
        let message = app.querySelector(".chat-screen #message-input").value;
        if(message.length == 0){
            return;
        }
        // mereka akan menerima json disini
        renderMessage("me",{
            username:uname, 
            text:message
        });
        // kirim pesannya ke index.js menggunakan socket
        socket.emit("prompt",{ 
            username:uname,
            text:message
        });
    });

    // bikin penerimaan nya dari index.js ke script.js melalui socket.io / mengambil codingan chatbot nya
    socket.on("chatbot", function(message){
        // console.log(message);
        renderMessage("bot",message);
    });


    // membuat objek nya dijavascript
    // membuat booble chat nya
    // .type berfungsi balasan chat dari bot nya tampil di sebelah kiri
    function renderMessage(type,message){
        let messageContainer = app.querySelector(".chat-screen .messages");
        // membuat element barunya
        if(type == "me"){
            let el = document.createElement("div");
            el.setAttribute("class", "message my-message");
            // menggunakan back tip agar bisa menaro simbol dolar
            el.innerHTML = `
                    <div>
                            <div class="name">You:${message.username} </div>
                            <div class="text">${message.text}</div>
                    <div>
            `;
            messageContainer.appendChild(el);
        // else membuat rata kiri nya
        }else if(type == "bot"){
            let el = document.createElement("div");
            // sesuai yang ada di css nya .other-message
            el.setAttribute("class", "message other-message");
            el.innerHTML = `
                    <div>
                            <div class="name">${message.username} </div>
                            <div class="text">${message.text}</div>
                    <div>
            `;
            messageContainer.appendChild(el);
        }
    }
})();